ADB = $(shell which adb 2> /dev/null)
ifeq (${ADB},)
ADB := /mnt/g/Misc/AndroidStudioSDK/platform-tools/adb.exe
endif

APKANALYZER = $(shell which apkanalyzer 2> /dev/null)
ifeq (${APKANALYZER},)
APKANALYZER := /mnt/g/Misc/AndroidStudioSDK/tools/bin/apkanalyzer
endif

ifeq (${ANDROID_NDK_HOME},)
ANDROID_NDK_HOME := "$(shell pwd)/android-ndk-r21d"
endif

release:
	ANDROID_NDK_HOME=$(ANDROID_NDK_HOME) $(shell go env GOPATH)/bin/fyne package -os android -release -icon assets/icon.png -appID com.gitlab.rigel314.vibenet

debug:
	ANDROID_NDK_HOME=$(ANDROID_NDK_HOME) $(shell go env GOPATH)/bin/fyne package -os android/arm64    -icon assets/icon.png -appID com.gitlab.rigel314.vibenet

install:
	${ADB} install vibenet.apk

run:
	${ADB} shell am start -a android.intent.action.MAIN -n com.gitlab.rigel314.vibenet/org.golang.app.GoNativeActivity

instrun: install run

extract-manifest:
	$(APKANALYZER) manifest print vibenet.apk

.PHONY: release debug install run instrun extract-manifest
