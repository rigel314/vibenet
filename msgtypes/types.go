package msgtypes

import (
	"encoding/binary"
)

// MsgType holds a message type enum
type MsgType uint32

// Message types
const (
	MsgInval MsgType = iota
	MsgPing
	MsgPong
	MsgStart
	MsgStop
	MsgTurn
)

// BaseMsg is the header on all messages
type BaseMsg struct {
	Sync uint32
	MsgType
}

// MagicSync is a special pattern at the beginning of each message as an extra form of validation
const MagicSync = 0xC8D99057

var defaultBaseMsg = BaseMsg{Sync: MagicSync}

// PingMsg holds a Ping message
type PingMsg struct {
	BaseMsg
	Val uint32
}

// PongMsg holds a Pong message
type PongMsg struct {
	BaseMsg
	Val uint32
}

// StartMsg holds a Start message
type StartMsg struct {
	BaseMsg
	Off      uint32
	On       uint32
	Strength uint8
}

// StopMsg holds a Stop message
type StopMsg struct {
	BaseMsg
}

// TurnMsg holds a Turn message
type TurnMsg struct {
	BaseMsg
}

// Message Sizes
var (
	BaseMsgSize  = binary.Size(BaseMsg{})
	PingMsgSize  = binary.Size(PingMsg{})
	PongMsgSize  = binary.Size(PongMsg{})
	StartMsgSize = binary.Size(StartMsg{})
	StopMsgSize  = binary.Size(StopMsg{})
	TurnMsgSize  = binary.Size(TurnMsg{})
)

// The Msg interface has a helper function to get the message header
type Msg interface {
	Header() BaseMsg
}

// Header returns the header of any message which embeds BaseMsg
func (b BaseMsg) Header() BaseMsg {
	return b
}

// Header returns a BaseMsg of the given MsgType
func Header(t MsgType) BaseMsg {
	// log.Println("building msg: ", t)
	b := defaultBaseMsg
	b.MsgType = t
	return b
}
