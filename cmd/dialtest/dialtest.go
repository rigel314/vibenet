// This tests a double close of a tcp client connection, it's allowed and works.
package main

import (
	"log"
	"net"
	"sync"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	wg := new(sync.WaitGroup)
	wg.Add(1)
	go listen(wg)
	wg.Wait()

	s, err := net.DialTCP("tcp", nil, &net.TCPAddr{
		IP:   net.ParseIP("127.0.0.1"),
		Port: 7082,
	})
	if err != nil {
		log.Fatal(err)
	}
	log.Println(s.Close())
	log.Println(s.Close())
	log.Println("here")
}

func listen(wg *sync.WaitGroup) {
	l, err := net.ListenTCP("tcp", &net.TCPAddr{
		IP:   net.ParseIP("127.0.0.1"),
		Port: 7082,
	})
	if err != nil {
		log.Fatal(err)
	}

	wg.Done()

	c, err := l.Accept()
	if err != nil {
		log.Fatal(err)
	}

	buf := make([]byte, 1024)
	c.Read(buf)
}
