package main

// import (
// 	"bytes"
// 	"encoding/binary"
// 	"log"
// 	"net"
// 	"time"
// )

// // MsgType holds a message type enum
// type MsgType uint32

// // Message types
// const (
// 	MsgInval MsgType = iota
// 	MsgPing
// 	MsgPong
// 	MsgStart
// 	MsgStop
// 	MsgTurn
// )

// // BaseMsg is the header on all messages
// type BaseMsg struct {
// 	Sync    uint32
// 	Channel uint32
// 	MsgType
// }

// const magicSync = 0xC8D99057

// var defaultBaseMsg = BaseMsg{Sync: magicSync}

// type pingMsg struct {
// 	BaseMsg
// 	Val uint32
// }

// var (
// 	baseMsgSize = binary.Size(BaseMsg{})
// 	pingMsgSize = binary.Size(pingMsg{})
// )

// type udpHeader struct {
// 	Sport, Dport, Len, Ck uint16
// }

// type pinger struct {
// 	addr net.UDPAddr
// 	t    time.Time
// }

// func main() {
// 	// create listener
// 	laddr, err := net.ResolveUDPAddr("udp", ":61592")
// 	if err != nil {
// 		log.Println("failed to resolve local IP: ", err)
// 		return
// 	}
// 	lconn, err := net.ListenUDP("udp", laddr)
// 	if err != nil {
// 		log.Println("failed to listen: ", err)
// 		return
// 	}
// 	defer lconn.Close()

// 	dests := make(map[string]pinger)
// 	buf := make([]byte, 1024)
// 	for {
// 		// read a message
// 		n, raddr, err := lconn.ReadFromUDP(buf)
// 		if err != nil {
// 			log.Println("Failed udp read: ", err)
// 			continue
// 		}
// 		msg := buf[:n]
// 		msgR := bytes.NewReader(msg)
// 		if n < baseMsgSize {
// 			log.Println("size too small from: ", raddr)
// 			continue
// 		}

// 		// interpret message header
// 		var basemsg BaseMsg
// 		err = binary.Read(msgR, binary.BigEndian, &basemsg)
// 		if err != nil {
// 			log.Println("read header failed: ", err)
// 			continue
// 		}
// 		if basemsg.Sync != magicSync {
// 			log.Println("invalid sync from: ", raddr)
// 			continue
// 		}

// 		// reset reader, so each message can call binary.Read()
// 		msgR.Reset(msg)
// 		// handle each message type
// 		log.Println("got", n, "bytes from", raddr, ", msgID:", basemsg.MsgType, ", ch:", basemsg.Channel)
// 		raddr.Port = 61592
// 		switch basemsg.MsgType {
// 		case MsgPing:
// 			if n < pingMsgSize {
// 				log.Println("header says ping, but msg too small: ", raddr)
// 				continue
// 			}
// 			var pingmsg pingMsg
// 			err = binary.Read(msgR, binary.BigEndian, &pingmsg)
// 			if err != nil {
// 				log.Println("read ping failed: ", err)
// 				continue
// 			}

// 			dests[raddr.IP.String()] = pinger{*raddr, time.Now()}
// 			sendToKnown(dests, msg)
// 		default:
// 			log.Println("invalid msgType:", basemsg.MsgType)
// 		}
// 	}
// }

// func sendToKnown(dests map[string]pinger, msg []byte) {
// 	// send pong as response
// 	client, err := net.DialUDP("udp", nil, raddr)
// 	if err != nil {
// 		log.Println("failed to dial response addr: ", err)
// 		return
// 	}
// 	pongmsg := pongMsg{
// 		BaseMsg: header(MsgPong),
// 		Val:     pingmsg.Val,
// 	}
// 	err = binary.Write(resp, binary.BigEndian, pongmsg)
// 	if err != nil {
// 		log.Println("failed to marshal pongmsg")
// 		continue
// 	}
// 	_, err = resp.WriteTo(client)
// 	if err != nil {
// 		log.Println("failed sendto pong")
// 	}
// 	client.Close()
// }
