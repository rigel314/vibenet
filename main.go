package main

import (
	"encoding/gob"
	"fmt"
	"log"
	"net"
	"strconv"
	"sync/atomic"
	"time"

	"fyne.io/fyne/app"
	"fyne.io/fyne/layout"
	"fyne.io/fyne/widget"
	"gitlab.com/rigel314/go-androidutils"
	"gitlab.com/rigel314/vibenet/msgtypes"
)

const pingMs = 30000

const (
	prefKeyPartner  = "partnerip"
	prefKeyOff      = "offtime"
	prefKeyOn       = "ontime"
	prefKeyStrength = "vibestrength"
)

var localAddr net.IP

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	a := app.NewWithID("com.gitlab.rigel314.vibenet")
	w := a.NewWindow("Vibenet")

	var sRunning bool
	var sStrength uint32 = 255
	var sOn uint32
	var sOff uint32

	stateMachineIn := make(chan interface{}) // must be unbuffered

	localAddr = findAddr()
	localAddrStr := ""
	if localAddr == nil {
		log.Println("failed finding local addr, using 0.0.0.0")
		localAddr = net.IPv4(0, 0, 0, 0)
		localAddrStr = "N/A"
	} else {
		localAddrStr = localAddr.String()
	}

	go listen(stateMachineIn)

	// foundLabel := widget.NewLabel(fmt.Sprintf("%d", 0))

	onPeriodEntry := widget.NewEntry()
	onPeriodEntry.SetText(fmt.Sprintf("%d", a.Preferences().IntWithFallback(prefKeyOn, 100)))
	onPeriodEntry.Validator = numericInput

	offPeriodEntry := widget.NewEntry()
	offPeriodEntry.SetText(fmt.Sprintf("%d", a.Preferences().IntWithFallback(prefKeyOff, 400)))
	offPeriodEntry.Validator = numericInput

	ipEntry := widget.NewEntry()
	ipEntry.SetText(a.Preferences().StringWithFallback(prefKeyPartner, "192.168.0.1"))
	ipEntry.Validator = ipInput

	strengthSlider := widget.NewSlider(1, 255)
	strengthSlider.Step = 1
	strengthSlider.SetValue(a.Preferences().FloatWithFallback(prefKeyStrength, 255))
	strengthSlider.OnChanged = func(_ float64) {
		if sRunning {
			strengthSlider.SetValue(float64(atomic.LoadUint32(&sStrength)))
		}
		atomic.StoreUint32(&sStrength, uint32(strengthSlider.Value))
	}

	sysuiBut := widget.NewButton("Open System UI Tuner", func() {
		androidutils.Vibrate(100*time.Millisecond, 255)
		err := androidutils.StartActivity("com.android.systemui/com.android.systemui.DemoMode")
		if err != nil {
			log.Println("err", err)
		}
	})
	goBut := widget.NewButton("Go", func() {
		stateMachineIn <- inputGo{}
	})
	stopBut := widget.NewButton("Stop", func() {
		stateMachineIn <- inputStop{}
	})
	stopBut.Disable()
	saveBut := widget.NewButton("Save", func() {
		if ipEntry.Validate() == nil &&
			onPeriodEntry.Validate() == nil &&
			offPeriodEntry.Validate() == nil {
			a.Preferences().SetString(prefKeyPartner, ipEntry.Text)

			offper, _ := strconv.Atoi(offPeriodEntry.Text)
			a.Preferences().SetInt(prefKeyOff, offper)

			onper, _ := strconv.Atoi(onPeriodEntry.Text)
			a.Preferences().SetInt(prefKeyOn, onper)

			val := float64(uint8(atomic.LoadUint32(&sStrength)))
			if val == 0 {
				val = 1
			}
			a.Preferences().SetFloat(prefKeyStrength, val)
		}
	})
	ipLabel := widget.NewLabel(localAddrStr)

	w.SetContent(widget.NewVBox(
		sysuiBut,
		widget.NewSeparator(),
		widget.NewHBox(
			widget.NewLabel("Partner:"),
			ipEntry,
			layout.NewSpacer(),
			widget.NewLabel("This Device:"),
			ipLabel,
		),
		widget.NewSeparator(),
		widget.NewLabel("Period:"),
		widget.NewHBox(
			widget.NewLabel("On:"),
			onPeriodEntry,
			widget.NewLabel("Off:"),
			offPeriodEntry,
		),
		widget.NewSeparator(),
		widget.NewLabel("Strength:"),
		strengthSlider,
		widget.NewSeparator(),
		widget.NewHBox(
			goBut,
			stopBut,
			layout.NewSpacer(),
			saveBut,
		),
	))

	disable := func() {
		sysuiBut.Disable()
		saveBut.Disable()
		goBut.Disable()
		onPeriodEntry.Disable()
		offPeriodEntry.Disable()
		ipEntry.Disable()
		// strengthSlider.Disable()
		stopBut.Enable()
	}
	enable := func() {
		sysuiBut.Enable()
		saveBut.Enable()
		goBut.Enable()
		onPeriodEntry.Enable()
		offPeriodEntry.Enable()
		ipEntry.Enable()
		// strengthSlider.Enable()
		stopBut.Disable()
	}

	// state machine input handler
	go func() {
		var dest *inputConnect
		timer := time.NewTimer(pingMs * time.Millisecond)
		for {
			// log.Println("before select")
			select {
			case in := <-stateMachineIn:
				// log.Printf("got %T\n", in)
				switch v := in.(type) {
				case inputPong:
					// log.Println("got ping/pong from: ", v.addr.IP.String())
					ipEntry.SetText(v.addr.IP.String())
					// foundLabel.SetText(fmt.Sprintf("%d", len(pongs)))
				case msgtypes.StartMsg:
					if sRunning {
						log.Println("startMsg already running")
						continue
					}
					disable()
					sRunning = true
					sOff = v.Off
					sOn = v.On
					atomic.StoreUint32(&sStrength, uint32(v.Strength))
				case msgtypes.StopMsg:
					if !sRunning {
						log.Println("stopMsg not running yet")
						continue
					}
					enable()
				case msgtypes.TurnMsg:
					if !sRunning {
						log.Println("turnMsg not running yet")
						continue
					}
					smsg := msgtypes.StartMsg{
						Off:      sOff,
						On:       sOn,
						Strength: uint8(atomic.LoadUint32(&sStrength)),
					}
					go vibe(stateMachineIn, dest, smsg)
				case inputGo:
					if sRunning {
						log.Println("inputGo already running")
						continue
					}
					newdest := dial(stateMachineIn, ipEntry.Text)
					if newdest != nil {
						dest = newdest
					} else {
						log.Println("go button dial failed")
						continue
					}
					disable()
					sRunning = true
					_sOff, _ := strconv.Atoi(offPeriodEntry.Text)
					_sOn, _ := strconv.Atoi(onPeriodEntry.Text)
					sOff, sOn = uint32(_sOff), uint32(_sOn)
					smsg := msgtypes.StartMsg{
						Off:      sOff,
						On:       sOn,
						Strength: uint8(atomic.LoadUint32(&sStrength)),
					}
					if dest != nil && dest.start(smsg) {
						dest = nil
					}
					go vibe(stateMachineIn, dest, smsg)
				case inputStop:
					if !sRunning {
						log.Println("inputStop not running")
						continue
					}
					sRunning = false
					enable()
					if dest != nil && dest.stop() {
						dest = nil
					}
					if dest != nil {
						dest.close()
					}
				case *inputConnect:
					dest = v
				case inputDisconnect:
					dest = nil
					go func() {
						stateMachineIn <- inputStop{}
					}()
				default:
					log.Printf("unhandled statemachine input type: %T\n", in)
				}
				if !timer.Stop() {
					<-timer.C
				}
			case <-timer.C:
				// log.Println("got timer.C")
				if ipEntry.Validate() == nil && dest != nil {
					if dest.ping() {
						dest = nil
					}
				}
			}

			timer.Reset(pingMs * time.Millisecond)
		}
	}()

	w.ShowAndRun()
}

type inputGo struct{}
type inputStop struct{}
type inputChannelChange struct {
	newCh uint32
}
type inputPong struct {
	addr *net.TCPAddr
	t    time.Time
}
type inputConnect struct {
	send  *gob.Encoder
	close func()
}
type inputDisconnect struct{}

func numericInput(in string) error {
	_, err := strconv.Atoi(in)
	return err
}

func ipInput(in string) error {
	ip := net.ParseIP(in)
	if ip == nil {
		return fmt.Errorf("bad ip")
	}
	return nil
}

func vibe(stateMachineIn chan interface{}, dest *inputConnect, smsg msgtypes.StartMsg) {
	// ch := make(chan struct{})
	// go func() {
	// 	time.Sleep(time.Duration(smsg.Off+smsg.On) * time.Millisecond)
	// 	ch <- struct{}{}
	// }()
	ch := time.After(time.Duration(smsg.Off+smsg.On) * time.Millisecond)
	androidutils.Vibrate(time.Duration(smsg.On)*time.Millisecond, smsg.Strength)
	<-ch
	if dest != nil && dest.turn() {
		stateMachineIn <- inputDisconnect{}
	}
}
