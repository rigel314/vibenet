module gitlab.com/rigel314/vibenet

go 1.18

require (
	fyne.io/fyne v1.4.2
	gitlab.com/rigel314/go-androidutils v0.0.3-0.20220523052447-6747ee6c6c34
)

require (
	git.wow.st/gmp/jni v0.0.0-20210610011705-34026c7e22d0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/fyne-io/mobile v0.1.2-0.20201127155338-06aeb98410cc // indirect
	github.com/go-gl/gl v0.0.0-20190320180904-bf2b1f2f34d7 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20200625191551-73d3c3675aa3 // indirect
	github.com/godbus/dbus/v5 v5.0.3 // indirect
	github.com/goki/freetype v0.0.0-20181231101311-fa8a33aabaff // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/srwiley/oksvg v0.0.0-20200311192757-870daf9aa564 // indirect
	github.com/srwiley/rasterx v0.0.0-20200120212402-85cb7272f5e9 // indirect
	github.com/stretchr/testify v1.5.1 // indirect
	gitlab.com/rigel314/go-androidlib v0.0.0-20220523051535-e88e2ecd5aa5 // indirect
	golang.org/x/image v0.0.0-20200430140353-33d19683fad8 // indirect
	golang.org/x/mobile v0.0.0-20190923204409-d3ece3b6da5f // indirect
	golang.org/x/net v0.0.0-20200324143707-d3edc9973b7e // indirect
	golang.org/x/sys v0.0.0-20200720211630-cb9d2d5c5666 // indirect
	golang.org/x/text v0.3.2 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)

replace golang.org/x/mobile => github.com/fyne-io/gomobile-bridge v0.0.1

replace git.wow.st/gmp/jni => gitlab.com/rigel314/go-jni v0.0.0-20220523051044-07a3472a6b30

// replace gitlab.com/rigel314/go-androidutils => ../go-androidutils

// replace gitlab.com/rigel314/go-androidlib => ../go-androidlib

// replace git.wow.st/gmp/jni => ../jni

// replace fyne.io/fyne => ../fyne
