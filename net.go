package main

import (
	"encoding/gob"
	"log"
	"math/rand"
	"net"
	"time"

	"gitlab.com/rigel314/vibenet/msgtypes"
	"gitlab.com/rigel314/vibenet/privateip"
)

type wrap struct {
	V msgtypes.Msg
}

// TODO: find wlan0 IP and use it for local addr on Dials and Listens
func findAddr() net.IP {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Println("list interfaces error:", err)
		return nil
	}
	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			log.Println("list addrs for interface:", iface.Name, "error:", err)
			return nil
		}
		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if !privateip.IsPrivateIP(ip) {
				continue
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			return ip
		}
	}
	return nil
}

func init() {
	gob.Register(msgtypes.BaseMsg{})
	gob.Register(msgtypes.PingMsg{})
	gob.Register(msgtypes.PongMsg{})
	gob.Register(msgtypes.StartMsg{})
	gob.Register(msgtypes.StopMsg{})
	gob.Register(msgtypes.TurnMsg{})
}

func listen(stateMachineIn chan interface{}) {
	// create listener
	lconn, err := net.ListenTCP("tcp", &net.TCPAddr{
		IP:   localAddr,
		Port: 61592,
	})
	if err != nil {
		log.Println("failed to listen: ", err)
		return
	}
	defer lconn.Close()

	// accept loop
	for {
		client, err := lconn.AcceptTCP()
		if err != nil {
			log.Println("accept error: ", err)
			continue
		}

		msgDecoder := gob.NewDecoder(client)
		msgEncoder := gob.NewEncoder(client)

		stateMachineIn <- &inputConnect{
			send: msgEncoder,
			close: func() {
				client.Close()
			},
		}

		recvHandler(stateMachineIn, client, msgDecoder, msgEncoder, func() {
			stateMachineIn <- inputDisconnect{}
			client.Close()
		})
	} // accept loop
}

func dial(stateMachineIn chan interface{}, target string) *inputConnect {
	svr, err := net.DialTCP("tcp", nil, &net.TCPAddr{
		IP:   net.ParseIP(target),
		Port: 61592,
	})
	if err != nil {
		log.Println("dial failed: ", err)
		return nil
	}

	msgDecoder := gob.NewDecoder(svr)
	msgEncoder := gob.NewEncoder(svr)

	go func() {
		recvHandler(stateMachineIn, svr, msgDecoder, msgEncoder, func() {
			stateMachineIn <- inputDisconnect{}
			svr.Close()
		})
	}()

	return &inputConnect{
		send: msgEncoder,
		close: func() {
			svr.Close()
		},
	}
}

func recvHandler(stateMachineIn chan interface{}, sock *net.TCPConn, recv *gob.Decoder, send *gob.Encoder, close func()) {
	// TODO: add timeout to allow this to start working again if nothing ever shows up

	defer close()

	// message loop
	for {
		// read a message
		var w wrap
		err := recv.Decode(&w)
		if err != nil {
			log.Println("failed decode: ", err)
			return
		}
		if w.V.Header().Sync != msgtypes.MagicSync {
			log.Println("invalid sync")
			continue
		}

		// handle each message type
		switch msg := w.V.(type) {
		case msgtypes.PingMsg:
			pongmsg := wrap{
				V: &msgtypes.PongMsg{
					BaseMsg: msgtypes.Header(msgtypes.MsgPong),
					Val:     msg.Val,
				},
			}
			err := send.Encode(&pongmsg)
			if err != nil {
				log.Println("failed to encode pongmsg")
				return
			}
			stateMachineIn <- inputPong{
				addr: sock.RemoteAddr().(*net.TCPAddr),
				t:    time.Now(),
			}
		case msgtypes.PongMsg:
			stateMachineIn <- inputPong{addr: sock.RemoteAddr().(*net.TCPAddr), t: time.Now()}
		case msgtypes.StartMsg:
			stateMachineIn <- msg
		case msgtypes.StopMsg:
			stateMachineIn <- msg
		case msgtypes.TurnMsg:
			stateMachineIn <- inputPong{
				addr: sock.RemoteAddr().(*net.TCPAddr),
				t:    time.Now(),
			}
			stateMachineIn <- msg
		default:
			log.Printf("invalid msgType: %T\n", msg)
		}
	} // message loop
}

func (ic *inputConnect) ping() (failed bool) {
	pingmsg := wrap{
		V: &msgtypes.PingMsg{
			BaseMsg: msgtypes.Header(msgtypes.MsgPing),
			Val:     rand.Uint32(),
		},
	}
	err := ic.send.Encode(&pingmsg)
	if err != nil {
		log.Println("failed encode ping: ", err)
		ic.close()
		return true
	}

	return false
}

func (ic *inputConnect) stop() (failed bool) {
	stopmsg := wrap{
		V: &msgtypes.StopMsg{
			BaseMsg: msgtypes.Header(msgtypes.MsgStop),
		},
	}
	err := ic.send.Encode(&stopmsg)
	if err != nil {
		log.Println("failed encode stop: ", err)
		ic.close()
		return true
	}

	return false
}

func (ic *inputConnect) start(s msgtypes.StartMsg) (failed bool) {
	startmsg := wrap{
		V: &msgtypes.StartMsg{
			BaseMsg:  msgtypes.Header(msgtypes.MsgStart),
			Off:      s.Off,
			On:       s.On,
			Strength: s.Strength,
		},
	}
	err := ic.send.Encode(&startmsg)
	if err != nil {
		log.Println("failed encode start: ", err)
		ic.close()
		return true
	}

	return false
}

func (ic *inputConnect) turn() (failed bool) {
	turnmsg := wrap{
		V: &msgtypes.TurnMsg{
			BaseMsg: msgtypes.Header(msgtypes.MsgTurn),
		},
	}
	err := ic.send.Encode(&turnmsg)
	if err != nil {
		log.Println("failed sendto turn: ", err)
		ic.close()
		return true
	}

	return false
}
